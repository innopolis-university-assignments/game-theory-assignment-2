/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
package gametheory.assignment2.students2021;

import gametheory.assignment2.Player;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

import static java.lang.Math.max;

/**
 * Agent implementation of Player interface for Moose game.
 * It use Cooperate or Punish strategy
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class CooperateOrPunishPlayer implements Player {
    LinkedList<Integer> opponentLastMoves;  // Stores opponent's moves
    LinkedList<Integer> myLastMoves;  // Store agent's moves
    LinkedList<Integer> a_score;  // Store a field values
    LinkedList<Integer> b_score;  // Store b field values
    LinkedList<Integer> c_score;  // Store c field values
    Random random;
    int my_move;
    int counter;
    boolean flag;

    /**
     * Constructor of the class
     */
    public CooperateOrPunishPlayer() {
        this.opponentLastMoves = new LinkedList<>();
        this.myLastMoves = new LinkedList<>();
        this.a_score = new LinkedList<>();
        this.b_score = new LinkedList<>();
        this.c_score = new LinkedList<>();
        this.random = new Random();
        this.my_move = random.nextInt(3) + 1;
        this.counter = 0;
        this.flag = true;
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {
        this.opponentLastMoves.clear();
        this.myLastMoves.clear();
        this.a_score.clear();
        this.b_score.clear();
        this.c_score.clear();
        this.my_move = random.nextInt(3) + 1;
        this.counter = 0;
        this.flag = true;
    }

    /**
     * This method returns the max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getMaxField(int xA, int xB, int xC) {
        if (xA >= max(xB, xC)) {
            if (xA == max(xB, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 1;
                } else if (xB > xC) {
                    return 2;
                } else {
                    return 3;
                }

            } else {
                return 1;  // If xA is absolute max
            }
        }
        if (xB >= max(xA, xC)) {
            if (xB == max(xA, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 2;
                } else {
                    return 3;
                }
            } else {  // If xB is absolute max
                return 2;
            }

        }
        return 3;  // Return xC as it is absolute max
    }

    /**
     * This method returns the second max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return second max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getSecondMaxField(int xA, int xB, int xC) {
        if (xA >= max(xB, xC)) {
            if (xB == max(xB, xC)) { // Return the second max field
                return 2;
            } else {
                return 3;
            }
        }
        if (xB >= max(xA, xC)) {
            if (xA == max(xA, xC)) { // Return the second max field
                return 1;
            } else {
                return 3;
            }
        }
        // if xC is absolute max
        if (xA == max(xA, xB)) { // Return the second max field
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * This method returns True if opponents plays as Greedy
     *
     * @return boolean
     */
    private boolean isGreedy() {
        boolean flag2 = false;
        int cnt = 0;
        for (int i = 0; i < opponentLastMoves.size(); i++) {
            if (opponentLastMoves.get(i) == getMaxField(a_score.get(i),
                    b_score.get(i), c_score.get(i))) {
                cnt++;
            } else {
                cnt = 0;
                flag2 = false;
            }

            if (cnt > 5) {
                flag2 = true;
            }
        }

        return flag2;
    }

    /**
     * This method returns True if opponents plays as Cooperator
     *
     * @return boolean
     */
    private boolean isCooperator() {
        int cnt1 = 0;
        int cnt2 = 0;
        for (int i = 0; i < opponentLastMoves.size(); i++) {
            if (opponentLastMoves.get(i) == getMaxField(a_score.get(i),
                    b_score.get(i), c_score.get(i))) {
                cnt1 = cnt1 + 1;
            } else {
                cnt2 = cnt2 + 1;
            }
        }
        return Math.abs(cnt1 - cnt2) < 3;
    }


    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        if (opponentLastMoves.size() > 5) {
            if (isCooperator()) {  // If opponent is cooperator
                if ((opponentLastMove == getMaxField(a_score.getLast(),
                        b_score.getLast(), c_score.getLast()))
                        && (opponentLastMove != myLastMoves.getLast())) {

                    my_move = getMaxField(xA, xB, xC);
                } else {
                    my_move = getSecondMaxField(xA, xB, xC);
                }
            } else if (isGreedy()) {  // If opponent is greedy
                my_move = getSecondMaxField(xA, xB, xC);
            } else {
                my_move = getMaxField(xA, xB, xC);
            }

        } else {
            my_move = getMaxField(xA, xB, xC);
        }

        // Store data from this round
        a_score.add(xA);
        b_score.add(xB);
        c_score.add(xC);
        opponentLastMoves.add(opponentLastMove);
        myLastMoves.add(my_move);
        return my_move;
    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Agent implementation of Player interface for Moose game.
 * It use "Cow" strategy
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class CowPlayer implements Player {
    Random random;
    int current;  // Current field

    /**
     * Constructor of the class
     */
    public CowPlayer() {
        this.random = new Random();
        this.current = random.nextInt(3) + 1;
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {
        this.current = random.nextInt(3) + 1;
    }

    private boolean isEmpty(int current, int xA, int xB, int xC) {
        if (current == 1) {
            return xA == 0;
        }
        if (current == 2) {
            return xB == 0;
        }
        if (current == 3) {
            return xC == 0;
        }

        return true;
    }

    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        if (this.current == 1 && (isEmpty(this.current, xA, xB, xC) || xA < max(xB, xC))) {
            if (xB == max(xB, xC)) {
                this.current = 2;
            } else {
                this.current = 3;
            }
        }
        if (this.current == 2 && (isEmpty(this.current, xA, xB, xC) || xB < max(xA, xC))) {
            if (xA == max(xA, xC)) {
                this.current = 1;
            } else {
                this.current = 3;
            }
        }
        if (this.current == 3 && (isEmpty(this.current, xA, xB, xC) || xC < max(xB, xA))) {
            if (xA == max(xA, xB)) {
                this.current = 1;
            } else {
                this.current = 2;
            }
        }

        return this.current;
    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Agent implementation of Player interface for Moose game.
 * It use Detective strategy
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class DetectivePlayer implements Player {
    LinkedList<Integer> opponentLastMoves;  // Stores opponent's moves
    LinkedList<Integer> myLastMoves;  // Store agent's moves
    LinkedList<Integer> a_score;  // Store a field values
    LinkedList<Integer> b_score;  // Store b field values
    LinkedList<Integer> c_score;  // Store c field values
    Random random;
    boolean flag;
    int my_move;

    /**
     * Constructor of the class
     */
    public DetectivePlayer() {
        this.opponentLastMoves = new LinkedList<>();
        this.myLastMoves = new LinkedList<>();
        this.a_score = new LinkedList<>();
        this.b_score = new LinkedList<>();
        this.c_score = new LinkedList<>();
        this.random = new Random();
        this.flag = true;
        this.my_move = random.nextInt(3) + 1;
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {
        this.opponentLastMoves.clear();
        this.myLastMoves.clear();
        this.a_score.clear();
        this.b_score.clear();
        this.c_score.clear();
        this.flag = true;
    }

    /**
     * This method returns the max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getMaxField(int xA, int xB, int xC) {
        if (xA >= max(xB, xC)) {
            if (xA == max(xB, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 1;
                } else if (xB > xC) {
                    return 2;
                } else {
                    return 3;
                }

            } else {
                return 1;  // If xA is absolute max
            }
        }
        if (xB >= max(xA, xC)) {
            if (xB == max(xA, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 2;
                } else {
                    return 3;
                }
            } else {  // If xB is absolute max
                return 2;
            }

        }
        return 3;  // Return xC as it is absolute max
    }

    /**
     * This method returns the second max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return second max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getSecondMaxField(int xA, int xB, int xC) {
        if (xA >= max(xB, xC)) {
            if (xB == max(xB, xC)) { // Return the second max field
                return 2;
            } else {
                return 3;
            }
        }
        if (xB >= max(xA, xC)) {
            if (xA == max(xA, xC)) { // Return the second max field
                return 1;
            } else {
                return 3;
            }
        }
        // if xC is absolute max
        if (xA == max(xA, xB)) { // Return the second max field
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        // This method tries to analyse field and choose the best strategy
        if (Math.max(xA, Math.max(xB, xC)) > 6) {
            // If some field has value bigger than 6 choose the max or second
            // max field, because it gives about the same payoff
            if (random.nextInt(2) == 1) {
                my_move = getSecondMaxField(xA, xB, xC);
            } else {
                my_move = getMaxField(xA, xB, xC);
            }
        } else {
            my_move = getMaxField(xA, xB, xC);  // Choose field: highest value
        }

        // Store data from this round
        a_score.add(xA);
        b_score.add(xB);
        c_score.add(xC);
        opponentLastMoves.add(opponentLastMove);
        myLastMoves.add(my_move);
        return my_move;
    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Agent implementation of Player interface for Moose game.
 * It use Greedy strategy
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class GreedyPlayer implements Player {
    Random random;

    /**
     * Constructor of the class
     */
    public GreedyPlayer() {
        this.random = new Random();
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {

    }

    /**
     * This method returns the max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getMaxField(int xA, int xB, int xC) {

        if (xA >= max(xB, xC)) {
            if (xA == max(xB, xC)) { // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 1;
                } else if (xB > xC) {
                    return 2;
                } else {
                    return 3;
                }

            } else {  // If xA is absolute max
                return 1;
            }
        }

        if (xB >= max(xA, xC)) {
            if (xB == max(xA, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 2;
                } else {
                    return 3;
                }
            } else {  // If xB is absolute max
                return 2;
            }

        }

        return 3;  // Return xC as it is absolute max
    }

    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        // This method returns field with max value
        return getMaxField(xA, xB, xC);
    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Agent implementation of Player interface for Moose game.
 * It use Random strategy
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class RandomPlayer implements Player {
    Random random;

    /**
     * Constructor of the class
     */
    public RandomPlayer() {
        this.random = new Random();
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {
    }

    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        // Return random integer from {1, 2, 3}
        return random.nextInt(3) + 1;
    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Agent implementation of Player interface for Moose game.
 * It use Tit for Tat strategy that avoid choosing blocked field
 *
 * @author Danil Usmanov
 * @version 1.1 11 March 2021
 */
class TitForTatPlayer implements Player {
    LinkedList<Integer> opponentLastMoves;  // Stores opponent's moves
    Random random;

    /**
     * Constructor of the class
     */
    public TitForTatPlayer() {
        this.opponentLastMoves = new LinkedList<>();
        this.random = new Random();
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {
        this.opponentLastMoves.clear();
    }

    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        // This method returns opponentLastMove if opponent doesn't
        // stay on the one field for long time,
        // otherwise return field with second max value
        if (opponentLastMove == 0) {
            return random.nextInt(3) + 1;
        }
        if ((this.opponentLastMoves.size() > 0)
                && (this.opponentLastMoves.getLast() != opponentLastMove)) {
            this.opponentLastMoves.add(opponentLastMove);
            return opponentLastMove;
        } else if (opponentLastMove == 1) {
            if (xB == max(xB, xC)) {
                return 2;
            } else {
                return 1;
            }
        } else if (opponentLastMove == 2) {
            if (xA == max(xA, xC)) {
                return 1;
            } else {
                return 3;
            }
        } else if (xA == max(xA, xB)) {
            return 1;
        } else {
            return 2;
        }


    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Agent implementation of Player interface for Moose game.
 * It use Smart cooperating strategy
 * It cooperates until opponent follow the rules
 * otherwise it plays as greedy
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class SmartCooperatePlayer implements Player {
    Random random;
    boolean flag;
    boolean isCooperatorFlag;
    int my_move;
    int myLastMoves;  // Store agent's last move
    int waiting_field;
    int play_field;
    int THRESHOLD;

    /**
     * Constructor of the class
     */
    public SmartCooperatePlayer() {
        this.random = new Random();
        this.flag = false;
        this.isCooperatorFlag = true;
        this.my_move = random.nextInt(3) + 1;
        this.myLastMoves = this.my_move;
        this.THRESHOLD = 4;
    }

    /**
     * This method is called to reset the agent before the match
     * with another player containing several rounds
     */
    @Override
    public void reset() {
        this.flag = false;
        this.isCooperatorFlag = true;
        this.my_move = random.nextInt(3) + 1;
        this.myLastMoves = this.my_move;
    }

    /**
     * This method returns the max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getMaxField(int xA, int xB, int xC) {
        if (xA >= max(xB, xC)) {
            if (xA == max(xB, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 1;
                } else if (xB > xC) {
                    return 2;
                } else {
                    return 3;
                }

            } else {
                return 1;  // If xA is absolute max
            }
        }
        if (xB >= max(xA, xC)) {
            if (xB == max(xA, xC)) {  // If there is more max values
                if (random.nextInt(2) == 1) {  // Randomly return some of them
                    return 2;
                } else {
                    return 3;
                }
            } else {  // If xB is absolute max
                return 2;
            }

        }
        return 3;  // Return xC as it is absolute max
    }

    /**
     * This method returns the second max field number of the fields based on
     * X values of all fields.
     * Initially, X for all fields is equal to 1.
     *
     * @param xA the argument X for a field A
     * @param xB the argument X for a field B
     * @param xC the argument X for a field C
     * @return second max field can be 1 for A, 2 for B
     * and 3 for C fields
     */
    private int getSecondMaxField(int xA, int xB, int xC) {
        if (xA >= max(xB, xC)) {
            if (xB == max(xB, xC)) { // Return the second max field
                return 2;
            } else {
                return 3;
            }
        }
        if (xB >= max(xA, xC)) {
            if (xA == max(xA, xC)) { // Return the second max field
                return 1;
            } else {
                return 3;
            }
        }
        // if xC is absolute max
        if (xA == max(xA, xB)) { // Return the second max field
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * This method returns True if opponents plays as Cooperator
     *
     * @return boolean
     */
    private boolean isCooperator(int opponentLastMove) {
        if (this.isCooperatorFlag) {
            this.isCooperatorFlag = opponentLastMove != this.play_field;
        }
        return this.isCooperatorFlag;
    }

    /**
     * This method returns the move of the player based on
     * the last move of the opponent and X values of all fields.
     * Initially, X for all fields is equal to 1 and last opponent
     * move is equal to 0
     *
     * @param opponentLastMove the last move of the opponent
     *                         varies from 0 to 3
     *                         (0 – if this is the first move)
     * @param xA               the argument X for a field A
     * @param xB               the argument X for a field B
     * @param xC               the argument X for a field C
     * @return the move of the player can be 1 for A, 2 for B
     * and 3 for C fields
     */
    @Override
    public int move(int opponentLastMove, int xA, int xB, int xC) {
        if (!flag && ((opponentLastMove == 0) ||
                (opponentLastMove == this.myLastMoves))) {
            my_move = random.nextInt(3) + 1;
        } else {
            if (!flag) {
                this.play_field = this.myLastMoves;
                // Calculate waiting field
                this.waiting_field = 6 - this.play_field - opponentLastMove;
                my_move = this.waiting_field;
                flag = true;
            } else {
                if (isCooperator(opponentLastMove)) {  // If opponent cooperates
                    // Wait until play field reaches 6 vegetation
                    my_move = this.waiting_field;
                    if ((this.play_field == 1) && (xA >= THRESHOLD)) {
                        my_move = this.play_field;
                    }

                    if ((this.play_field == 2) && (xB >= THRESHOLD)) {
                        my_move = this.play_field;
                    }

                    if ((this.play_field == 3) && (xC >= THRESHOLD)) {
                        my_move = this.play_field;
                    }
                } else {  // If opponent doesn't cooperate
                    if (Math.max(xA, Math.max(xB, xC)) > THRESHOLD) {
                        // If some field has value bigger than
                        // THRESHOLD choose the max or second
                        // max field, because it gives about the same payoff
                        if (random.nextInt(2) == 1) {
                            my_move = getSecondMaxField(xA, xB, xC);
                        } else {
                            my_move = getMaxField(xA, xB, xC);
                        }
                    } else {
                        my_move = getMaxField(xA, xB, xC);  // Choose field: highest value
                    }

                }
            }
        }

        this.myLastMoves = my_move;
        return my_move;
    }

    /**
     * This method returns my IU email
     *
     * @return my email
     */
    @Override
    public String getEmail() {
        return "d.usmanov@innopolis.university";
    }
}

/**
 * Field implementation for Moose game.
 * Stores field properties and actions for each of them
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class Field {
    int a;
    int b;
    int c;

    /**
     * Constructor of the class
     * Assign initial vegetation
     */
    public Field(int vegetation) {
        this.a = vegetation;
        this.b = vegetation;
        this.c = vegetation;
    }

    /**
     * This method returns the value of the field based on
     * field number.
     * Initially, X for all fields is equal to 1.
     *
     * @param field the argument represents field.
     *              1 for A, 2 for B and 3 for C fields.
     * @return the number of vegetation for field,
     * it can be 1 for A, 2 for B and 3 for C fields.
     */
    public int get_vegetation(int field) {
        if (field == 1) {
            return this.a;
        }
        if (field == 2) {
            return this.b;
        }

        return this.c;
    }

    /**
     * This method increases the value of the field based on
     * field number.
     *
     * @param field the argument represents field.
     *              1 for A, 2 for B and 3 for C fields.
     */
    private void increase_field(int field) {
        if (field == 1) {
            this.a = this.a + 1;
        }
        if (field == 2) {
            this.b = this.b + 1;
        }
        if (field == 3) {
            this.c = this.c + 1;
        }
    }

    /**
     * This method decreases the value of the field based on
     * field number.
     *
     * @param field the argument represents field.
     *              1 for A, 2 for B and 3 for C fields.
     */
    private void decrease_field(int field) {
        if ((field == 1) && (this.a > 0)) {
            this.a = this.a - 1;
        }
        if ((field == 2) && (this.b > 0)) {
            this.b = this.b - 1;
        }
        if ((field == 3) && (this.c > 0)) {
            this.c = this.c - 1;
        }
    }

    /**
     * This method updates the values of the fields based on
     * players moves.
     * Initially, X for all fields is equal to 1.
     *
     * @param move1 the first player move.
     * @param move2 the second player move.
     *              Can be 1 for A, 2 for B and 3 for C fields.
     */
    public void update_fields(int move1, int move2) {
        if ((move1 == 1) || (move2 == 1)) {  // If players stay on one field
            this.decrease_field(1);
        } else {
            this.increase_field(1);
        }
        if ((move1 == 2) || (move2 == 2)) {  // If players stay on one field
            this.decrease_field(2);
        } else {
            this.increase_field(2);
        }
        if ((move1 == 3) || (move2 == 3)) {  // If players stay on one field
            this.decrease_field(3);
        } else {
            this.increase_field(3);
        }
    }
}

/**
 * Game implementation of Moose game.
 * Regulates game rules and stores scores for each player
 *
 * @author Danil Usmanov
 * @version 1.0 11 March 2021
 */
class Game {
    Player player1;
    Player player2;
    Field field;
    double player1_score;
    double player2_score;
    public double player1_total_score;
    public double player2_total_score;
    int iterations;
    int rounds;

    /**
     * Constructor of the class
     * Assign players and initial values.
     */
    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.field = new Field(1);
        this.player1_score = 0;
        this.player2_score = 0;
        this.player1_total_score = 0;
        this.player2_total_score = 0;
        this.iterations = 1000;
        this.rounds = 100;
    }

    /**
     * This method calculates the payoff for the given x
     *
     * @param x is the number of vegetation.
     * @return the payoff.
     */
    public double payoff(int x) {
        return (10 * Math.pow(Math.E, x) / (1 + Math.pow(Math.E, x))) - 5.0;
    }

    /**
     * This method resets the players and game state.
     */
    public void reset() {
        this.player1.reset();
        this.player2.reset();
        this.field = new Field(1);
        this.player1_score = 0;
        this.player2_score = 0;
    }

    /**
     * This method starts the game for two players
     */
    public void start() {
        int player1_last_move = 0;
        int player2_last_move = 0;
        int p1_last_move_inner;
        int p2_last_move_inner;
        int p1_total_score = 0;
        int p2_total_score = 0;

        for (int i = 0; i < this.iterations; i++) {  // The number of games
            // We store previous moves of players because they will be
            // overwritten before usage.
            player1_last_move = 0;
            player2_last_move = 0;
            this.reset();
            for (int j = 0; j < this.rounds; j++) {  // The number of rounds
                p1_last_move_inner = player1_last_move;
                p2_last_move_inner = player2_last_move;

                player1_last_move = player1.move(p2_last_move_inner,
                        field.get_vegetation(1), field.get_vegetation(2),
                        field.get_vegetation(3));
                player2_last_move = player2.move(p1_last_move_inner,
                        field.get_vegetation(1), field.get_vegetation(2),
                        field.get_vegetation(3));

                if (player1_last_move != player2_last_move) {
                    this.player1_score += this.payoff(
                            field.get_vegetation(player1_last_move));
                    this.player2_score += this.payoff(
                            field.get_vegetation(player2_last_move));
                }

                field.update_fields(player1_last_move, player2_last_move);
            }

            p1_total_score += this.player1_score;
            p2_total_score += this.player2_score;
        }

        this.player1_total_score = p1_total_score / (double) this.iterations;
        this.player2_total_score = p2_total_score / (double) this.iterations;
    }

}

public class DanilUsmanovTesting {
    private LinkedList<Player> players;
    double[] players_result;
    Game game;

    public DanilUsmanovTesting() {
        this.players = new LinkedList<>();  // Create list of players

        /**
         *  For testing uncomment code below the online comment
         **/
        // Test 1 - tournament where participate only one agent instance
        this.players.add(new CooperateOrPunishPlayer());
        this.players.add(new CowPlayer());
        this.players.add(new TitForTatPlayer());
        this.players.add(new DetectivePlayer());
        this.players.add(new GreedyPlayer());
        this.players.add(new RandomPlayer());
        this.players.add(new SmartCooperatePlayer());

        // Test 2 - tournament where 50% is cooperators
//        this.players.add(new CooperateOrPunishPlayer());
//        this.players.add(new CowPlayer());
//        this.players.add(new TitForTatPlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new GreedyPlayer());
//        this.players.add(new RandomPlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());


        // Test 3 - tournament where SmartCooperatePlayer plays against
        // SmartCooperatePlayer
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());

        // Test 4 - tournament where SmartCooperatePlayer plays against
        // detective
//        this.players.add(new DetectivePlayer());
//        this.players.add(new SmartCooperatePlayer());

        // Test 5 - tournament where 10% SmartCooperatePlayer plays against
        // 90% detective
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new SmartCooperatePlayer());

        // Test 6 - tournament where 20% SmartCooperatePlayer plays against
        // 80% detective
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());

        // Test 7 - tournament where 30% SmartCooperatePlayer plays against
        // 70% detective
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());

        // Test 8 - tournament where 40% SmartCooperatePlayer plays against
        // 60% detective
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());

        // Test 9 - tournament where 50 % isSmartCooperatePlayer plays against
        // 50% detective
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new DetectivePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());
//        this.players.add(new SmartCooperatePlayer());

        tournament();  // Start tournament
    }

    public void add_player(Player player) {
        this.players.add(player);
    }


    public void tournament() {
        this.players_result = new double[this.players.size()];
        Arrays.fill(players_result, 0);

        for (int i = 0; i < this.players.size(); i++) {
            for (int j = this.players.size() - 1; j >= 0; j--) {
                if ((i == j) || (i > j)) {
                    continue;
                }
                this.game = new Game(this.players.get(i), this.players.get(j));
                this.game.reset();
                this.game.start();
                players_result[i] =
                        players_result[i] + this.game.player1_total_score;
                players_result[j] =
                        players_result[j] + this.game.player2_total_score;
            }
        }
        System.out.println("Results");
        for (int i = 0; i < this.players.size(); i++) {
            players_result[i] =
                    players_result[i] / (this.players.size() - 1);

            System.out.print(players.get(i).getClass().getSimpleName() + "  :");
            System.out.println(players_result[i]);
        }
    }
}